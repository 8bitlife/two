package main

import (
	"fmt"

	"gitlab.com/8bitlife/one/v2/fortag"
	"gitlab.com/8bitlife/one/v2/some"
)

func main() {
	fmt.Println(some.Var, fortag.Tag)
}
